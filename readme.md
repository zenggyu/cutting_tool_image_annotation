# 简介

本仓库用于管理刀具实例分割模型（[看这里](https://gitlab.com/zenggyu/cutting_tool_instance_segmentation)）所需的训练图像、标注数据及相关文件；此外，本库还将提供图像标注工具及相关教程，并被用作图像标注工作的协作平台。

以下是对本库所含主要文件和目录的简要描述：

- `via.html`文件：为图像标注工具VGG Image Annotator (VIA)的主程序文件。
- `attributes.json`文件：为VIA的属性配置文件。
- `img/`目录：用于存放需要标注的刀具图像。
- `annotation/`目录：用于存放上述图像的标注结果。

下文将对上述文件目录的作用和用法进行更详细的说明。

# 图像标注教程

[VIA（VGG Image Annotator）](https://gitlab.com/vgg/via/)是一款由英国牛津大学VGG计算机视觉研究组开发的开源图像标注工具。本教程将简要地介绍该工具在本项目中的基本使用方法；如需更进一步地了解该工具，请至其官方网站（[链接](http://www.robots.ox.ac.uk/~vgg/software/via/)）寻找相关信息。

## 1 准备工作

### 1.1 下载本库

请在本库首页（[页面链接](https://gitlab.com/zenggyu/cutting_tool_image_annotation/)）中找到下载按键，然后点击该按键并选择**Download zip**下载压缩包（见下图）。解压该压缩包后，可得到`cutting_tool_image_annotation-master/`目录，此目录内包含图像标注工作所需的所有文件。

注意：请勿修改仓库中任何目录和文件的名称，否则可能导致后续操作出错；如发现文件或目录存在问题，请在事项（issues）页面（[页面链接](https://gitlab.com/zenggyu/cutting_tool_image_annotation/issues/)）提交问题描述并提醒管理员处理。

![下载仓库](doc/fig/download_repo.png)

### 1.2 网页浏览器

本项目所使用的图像标注工具VIA是一个通过网页浏览器运行的程序；它已在下述三款浏览器中通过了测试：

- 火狐（[官网链接](http://www.firefox.com.cn/)）
- 谷歌（[官网链接](https://www.google.com/intl/zh-CN_ALL/chrome/)）
- Safari（[官网链接](https://www.apple.com/safari/)）

为避免程序出错，请使用上述浏览器运行VIA程序进行后续图像标注操作。

### 1.3 任务认领

为实现、协调多人同步标注，提高效率并避免对同一图像进行多次标注，标注工作将分开不同批次展开（每个批次均包含32幅图像）。请各位标注者了解批次划分及认领机制，并在标注前先对任务进行认领。

本仓库的事项（issues）页面列出了当前未被领取或已被领取但尚未完成的任务（见下图）。图像标注者在开展新批次的标注工作前，请先浏览带有`任务发布`标签的事项（[看这里](https://gitlab.com/zenggyu/cutting_tool_image_annotation/issues?label_name%5B%5D=%E4%BB%BB%E5%8A%A1%E5%8F%91%E5%B8%83)），找到尚未有人认领的任务，然后在事项中回复“认领”，再开始标注工作。已认领并且完成的事项将被关闭（[看这里](https://gitlab.com/zenggyu/cutting_tool_image_annotation/issues?state=closed&label_name[]=%E4%BB%BB%E5%8A%A1%E5%AE%8C%E6%88%90)）。

本库`annotation/`目录下存放了若干个`batch.*.json`文件，每个文件均包含一个批次的标注任务（文件名中的`*`为批次号：`001`, `002`, ...）。在进行图像标注时，找到与所认领的任务批次相对应的文件，并将其导入VIA（操作方法见后述），即可开始该批次的标注工作。

![任务认领](doc/fig/task.png)

## 2. 打开程序

用鼠标右键点击`via.html`文件，并使用火狐、谷歌或Safari浏览器打开，即可进入VIA的操作界面：

![打开工具](doc/fig/open_via.png)

## 3. 导入配置

配置文件`attributes.json`定义了刀具的分类及分类编号；在进行每个批次的图像标注工作之前，需要先将该配置文件导入VIA之中。具体步骤如下：

1. 在VIA操作界面的菜单栏依次点击**Project** → **Import region/file attributes**；
2. 在弹出的对话框中，找到`attributes.json`文件并将其导入。

导入成功后，工具栏**Attributes**下将显示刀具的分类编号及描述。

![导入配置](doc/fig/import_attributes.png)

## 4. 导入图像

以下是将图像导入VIA的操作步骤：

1. 在VIA操作界面的菜单栏依次点击**Annotation** → **Import Annotations (from json)**；
2. 在弹出的对话框中，找到与前述认领任务批次相对应的`batch.*.json`文件并将其导入。

下图演示了上述图像导入方法；导入成功后，操作界面中将显示出该批次所包含的图像。**注意：请不要同时导入多个批次的任务；若VIA中已存在任务，请先退出程序再重新导入新任务。**

![导入标注](doc/fig/import_annotations.png)

## 5. 图像标注

在标注前，先在VIA左侧工具栏**Region Shape**下选择**Polygon region shape**（多边形）作为标注工具（见下图）。本次工作的所有标注操作均使用该工具实现。

![形状](doc/fig/shape.png)

### 5.1 一般实例标注方法

**再次提醒：在进行图像标注前，请先确认该批次任务尚未被标注且亦未被其他人认领，否则将导致标注工作无效！！！**

一幅图像中可能包含多个类别刀具的多个实例；标注工作应以实例为单位进行。具体而言，在标注时，针对每个实例分别进行以下操作：

1. 沿实例边缘点击`鼠标左键`绘制多边形，使之包围该实例所覆盖的区域（若在点击过程中出现误操作，可按下`Backspace`按键撤销上一次点击操作；若想终止绘制操作，请按`Esc`键）；
2. 多边形绘制结束后，先按下`回车键`完成操作，再按下`Esc`键取消区域选定；
3. 用`鼠标左键`再次点击该区域，并在对话框中选择与实例相对应的分类；
4. 当绘制的区域被选中时，边缘将显示所标注的点；若对某些点的位置不满意，可用`鼠标左键`点击并拖动，进行调整；若对整个区域不满意，可在选中该区域后按下快捷键`d`将其删除，再重新绘制。

以下动图对上述步骤进行了演示：

![标注](doc/fig/annotate.gif)

### 5.2 特殊实例标注方法

剪刀、削皮刀等非实心物体带有以及被截断物体的标注方法较为特殊，此处对其进行特别讲解。

1. 首先，按一般实例标注方法描绘实例所覆盖的区域；
2. 然后，在选中区域的情况下，将鼠标对准多边形的线条（黄色），按下`Ctrl + 鼠标左键`添加多边形顶点（红色）；
3. 选中并拖动增加的顶点，对多边形形状进行调整；
4. 重复第2和第3步，直至最终使一个多边形能够完整覆盖一个实例且不包含杂物或背景。

下图演示了上述操作过程：

![标注剪刀](doc/fig/annotate_scissors.gif)

![标注截断物体](doc/fig/annotate_intercept.gif)

## 6. 保存结果

标注完成后，在菜单栏依次点击**Annotation** → **Export Annotations (as json)**，对结果进行保存。保存下来的文件默认命名为`via_region_data.json`，请对文件名进行修改，使之与任务导入时所使用的文件`batch.*.json`一致（见下图）。

若未完成标注，但需要保存当前结果留待下次继续，也可按上述方法保存结果；等到下次标注时，按前述任务导入方法将之前所保存的结果导入VIA即可继续。

![保存结果](doc/fig/save_results.png)

## 7. 提交结果

标注完成后，请到本库事项（issues）页面（[页面链接](https://gitlab.com/zenggyu/cutting_tool_image_annotation/issues/)）找到对应的任务批次，在其中回复“任务完成”，并在附件中添加标注结果文件`batch.*.json`（见下图）。

![提交](doc/fig/submit.png)

## 8. 附录

### 8.1 图像标注快捷键

下表给出了VIA程序所提供的所有快捷键：

![快捷键](doc/fig/shortcuts.png)
